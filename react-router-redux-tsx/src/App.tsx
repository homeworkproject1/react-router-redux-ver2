import React from 'react';
import logo from './logo.svg';
import './App.css';
import {
  Route,
  Link,
  Routes,
  BrowserRouter
} from "react-router-dom";
import HomePage from './pages/HomePage/HomePage';
import Login from './pages/Login/Login';
import Register from './pages/Register/Register';
import { Counter } from './counter/Counter';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route index element={<HomePage />}></Route>
        <Route path="Login" element={<Login />}></Route>
        <Route path="Register" element={<Register />}></Route>
        <Route path="*" element={<span>404</span>}></Route>
      </Routes>

      <Counter />

      <nav>
        <ul>
          <li>
            <Link to={'/'}>Home Page</Link>
            </li>
          <li>
            <Link to={'/Login'}>Login page</Link>
          </li>
          <li>
            <Link to={'/Register'}>Register page</Link>
          </li>
        </ul>
      </nav>
      

    </BrowserRouter>
  );
}

export default App;
